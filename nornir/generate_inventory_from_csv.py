#!./venv/bin/python3.8

from nornir import InitNornir

import csv
import yaml
import pandas as pd

CSV_FILE = 'inventory/switches.csv'
allowed_collumns = {'hostname', 'management_ip', 'username', 'password', 'platform'}
unique_columns = {'hostname', 'management_ip'}
required_collumns = {'hostname', 'management_ip'}

def read_and_validate_csv(csv_file):
    df = pd.read_csv(csv_file, delimiter=',')

    for column in required_collumns:
        if df[column].isnull().any():
            raise Exception(f'{column} column should have non-empty values in all rows')

    for column in unique_columns:
        if not df[column].is_unique:
            raise Exception(f'{column} column needs to be unique')

    if set(df.columns) != allowed_collumns:
        raise Exception(f'Incorrect column names, expected names are {allowed_collumns}')

        if df.shape[1] != len(allowed_collumns):
            raise Exception(f'Incorrect number of columns, collumns expected {len(allowed_collumns)}')

    return df

def generate_inventory_files():
    hosts = {}
    groups = {
        'vpc_defaults': {
            'data': {
                'vpc_peer': {
                    'vpc_peer_lag': 999,
                    'vpc_peer_lag_members': 'Ethernet1/5-6',
                    'vpc_peer_vlan': 999,
                    'vpc_peer_vlan_name': 'PEERLINK_VLAN',
                },
                'vpc_keepalive': {
                    'vpc_keepalive_vrf': 'VPC_KEEPALIVE',
                    'vpc_keepalive_lag': 1000,
                    'vpc_keepalive_lag_members': 'Ethernet1/7-8',
                    'vpc_keepalive_ip_subnet': '10.99.99.0/30',
                },
                'vpc_domain': {
                    'vpc_domain_id': 1,
                },
                'vpc_members' : [{
                    'vpc_member_lag_members': 'Ethernet1/1',
                    'vpc_member_lag': 1,
                    'vpc_member_vlan': 10,
                    'vpc_member_vpc': 1,
                    },
                    {
                    'vpc_member_lag_members': 'Ethernet1/2',
                    'vpc_member_lag': 2,
                    'vpc_member_vlan': 20,
                    'vpc_member_vpc': 2,
                    }
                ]
            }
        }
    }
    defaults = {
        'username': 'admin',
        'password': 'admin',
        'platform': 'nxos',
    }

    df = read_and_validate_csv(CSV_FILE);

    for index, row in df.iterrows():
        host_entry = {
            'hostname': row['management_ip'],
            'groups': [f'vPC_pair_{1 + len(hosts) // 2}'],
            'data': {
                'vpc_pair_id' : 1 + len(hosts) // 2,
                'vpc_pair_priority': 'primary' if index % 2 == 0 else 'secondary',
                'vpc_domain': {
                    'vpc_domain_id': 1,
                    'vpc_priority': 10 if index % 2 == 0 else 20,
                }
            }
        }

        if not pd.isna(row['username']):
            host_entry['username'] = row['username']

        if not pd.isna(row['password']):
            host_entry['password'] = row['password']

        if not pd.isna(row['platform']):
            host_entry['platform'] = row['platform']

        hosts[row['hostname']] = host_entry

    for i in range(1, len(hosts) // 2 + 2):
        groups[f'vPC_pair_{i}'] = {
            'groups' : ['vpc_defaults'],
            'data': {
                'vpc_pair_id': i
                }
        }

    with open('inventory/groups.yaml', 'w') as file:
        yaml.dump(groups, file)

    with open('inventory/hosts.yaml', 'w') as file:
        yaml.dump(hosts, file)

    with open('inventory/defaults.yaml', 'w') as file:
        yaml.dump(defaults, file)

    print("Inventory files generated successfully!")


def main():
    generate_inventory_files()

if __name__ == "__main__":
    main()
