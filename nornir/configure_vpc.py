from nornir import InitNornir
from nornir.core.task import Task, Result, AggregatedResult, MultiResult
import nornir_napalm.plugins.tasks as nn
from nornir.core.processor import Processor
from nornir.core.inventory import Host
from nornir_utils.plugins.functions import print_result

from jinja2 import Environment, FileSystemLoader
import ipaddress



env = Environment(loader=FileSystemLoader('.'))
NORNIR_CONFIG_FILE = 'config.yaml'

class PrintResult:
    def task_started(self, task: Task) -> None:
        print(f"vvv starting: {task.name}")

    def task_completed(self, task: Task, result: AggregatedResult) -> None:
        print(f"^^^ completed: {task.name}")

    def task_instance_started(self, task: Task, host: Host) -> None:
        # print(f"  - starting instance: {host.name}")
        pass

    def task_instance_completed(
        self, task: Task, host: Host, result: MultiResult
    ) -> None:
        changed_sum = sum(int(sub_result.changed) for sub_result in result)
        failed_sum = sum(int(sub_result.failed) for sub_result in result)
        print(f" >> {host.name} --- changed {changed_sum} --- failed {failed_sum} --- {task.name}")

    def subtask_instance_started(self, task: Task, host: Host) -> None:
        # print(f"     - starting subtask: {host.name}")
        pass

    def subtask_instance_completed(
        self, task: Task, host: Host, result: MultiResult
    ) -> None:
        print(f'  > {host.name} --- changed {int(result[0].changed)} --- failed {int(result[0].failed)} --- {task.name}')



def configure_vpc_members(task: Task, dry_run: bool = False) -> Result:
    for member in task.host['vpc_members']:

        template = env.get_template('templates/vpc_member.j2')
        task.run(
            name = f'Configure vPC member {member["vpc_member_vpc"]}',
            task = nn.napalm_configure,
            configuration = template.render(
                member = member
            ),
            dry_run = dry_run
        )

def configure_vpc(task: Task, dry_run: bool = False) -> Result:
    net = ipaddress.ip_network(task.host['vpc_keepalive']['vpc_keepalive_ip_subnet'])
    if net.prefixlen not in range(24,31):
        raise Exception('Only /24 to /30 subnets are allowed for the keepalive link')

    hosts = net.hosts()
    host1 = next(hosts)
    host2 = next(hosts)
    selected_host = host1 if task.host['vpc_pair_priority'] == 'primary' else host2

    result = MultiResult(name="configure_vpc")

    enable_features_result = task.run(
        name='Enable necessary features',
        task=nn.napalm_configure,
        configuration='feature vpc\nfeature lacp',
        dry_run = dry_run
    )
    result.append(enable_features_result)

    template = env.get_template('templates/vpc_keepalive.j2')
    vpc_keepalive_config = template.render(
        vpc_keepalive=task.host['vpc_keepalive'],
        vpc_keepalive_ip=f'{selected_host}/{net.prefixlen}'
    )
    vpc_keepalive_result = task.run(
        name='Configure vPC keepalive',
        task=nn.napalm_configure,
        configuration=vpc_keepalive_config,
        dry_run = dry_run
    )
    result.append(vpc_keepalive_result)

    template = env.get_template('templates/vpc_domain.j2')
    vpc_domain_config = template.render(
        vpc_domain=task.host['vpc_domain'],
        vpc_domain_src_ip=selected_host,
        vpc_domain_dst_ip=host2 if task.host['vpc_pair_priority'] == 'primary' else host1,
        vpc_domain_vrf=task.host['vpc_keepalive']['vpc_keepalive_vrf']
    )
    vpc_domain_result = task.run(
        name='Configure vPC domain',
        task=nn.napalm_configure,
        configuration=vpc_domain_config,
        dry_run = dry_run
    )
    result.append(vpc_domain_result)

    template = env.get_template('templates/vpc_peer.j2')
    vpc_peer_config = template.render(
        vpc_peer=task.host['vpc_peer']
    )
    vpc_peer_result = task.run(
        name='Configure vPC peer link',
        task=nn.napalm_configure,
        configuration=vpc_peer_config,
        dry_run = dry_run
    )
    result.append(vpc_peer_result)

    aggregated_result = AggregatedResult(result)
    aggregated_result["configure_vpc"] = result

    return aggregated_result


    # Commit confirm has not been implemented on this platform. Sadge
    # Would have to be moved to only be run once a pair has been configured anyway

    # result = task.run(
    #     name='Get vPC information',
    #     task=nn.napalm_cli,
    #     commands=['show vpc']
    # )
    #
    # if 'peer adjacency formed ok' in result['show vpc']:
    #     task.run(
    #         name = 'Confirm pending config',
    #         task = nn.napalm_confirm_commit
    #     )
    #     return Result(
    #         host = task.host,
    #         result = f'{task.host} has been configured with vPC.',
    #     )
    # else:
    #     task.run(
    #         name = 'Rollback pending config',
    #         task = nn.napalm_rollback
    #     )
    #     return Result(
    #         host = task.host,
    #         result = f'{task.host} did not form adjacency, rolling back config.',
    #     )





def main():
    init = InitNornir(config_file = NORNIR_CONFIG_FILE)
    nr = init.with_processors([PrintResult()])

    result = nr.run(name = 'configure_vPC', task = configure_vpc)
    # print_result(result,
    #     severity_level=logging.DEBUG
    #     )
    result = nr.run(name = 'configure_vPC_members', task = configure_vpc_members)
    # print_result(result,
    #     severity_level=logging.DEBUG
    #     )
    # print(nr.inventory.hosts['Switch1'].get_connection_parameters().dict())

if __name__ == "__main__":
    main()
