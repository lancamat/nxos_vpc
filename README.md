# nxos_vPC

## Description

Project for configuring vPC on pairs of Nexus9k switches.
Nornir with nornir_napalm.plugins
Ansible with cisco.nxos

## Usage

### Ansible

Edit the inventory.ini file with your hosts.
Possibly edit variables as needed in /vars and group_vars/ folders.
Run the playbooks.

```
ansible-playbook -i inventory.ini configure_vpc_playbook.yml
ansible-playbook -i inventory.ini configure_vpc_members_playbook.yml
```

### Nornir

Activate the python virtual environment with.

```
source venv/bin/activate
```

Edit inventory/switches.csv file with your switch inventory.

The csv has following restrictions:
allowed_collumns = {'hostname', 'management_ip', 'username', 'password', 'platform'}
unique_columns = {'hostname', 'management_ip'}
required_collumns = {'hostname', 'management_ip'}

```
echo "hostname,management_ip,password,platform,username" > inventory/switches.csv
echo "Switch1,10.0.0.1,,," >> inventory/switches.csv
echo "Switch2,10.0.0.2,,," >> inventory/switches.csv
```

Create template nornir SimpleInventory .yaml files from inventory/switches.csv you just created.

```
python generate_inventory_from_csv.py
```

The inventory files are pre-made with default values and stored in the inventory folder.
You can leverage the inheritance model of SimpleInventory to change specific values for each host/group individually.

Once you are happy with the values run the script configure_vpc.py
```
python configure_vpc.py
```


## Nornir Example output

```
(venv) ➜  nxos_vPC python configure_vpc.py
vvv starting: configure_vPC
  > Switch1 --- changed 0 --- failed 0 --- Enable necessary features
  > Switch2 --- changed 0 --- failed 0 --- Enable necessary features
  > Switch2 --- changed 1 --- failed 0 --- Configure vPC keepalive
  > Switch1 --- changed 1 --- failed 0 --- Configure vPC keepalive
  > Switch2 --- changed 1 --- failed 0 --- Configure vPC domain
  > Switch1 --- changed 1 --- failed 0 --- Configure vPC domain
  > Switch2 --- changed 1 --- failed 0 --- Configure vPC peer link
 >> Switch2 --- changed 3 --- failed 0 --- configure_vPC
  > Switch1 --- changed 1 --- failed 0 --- Configure vPC peer link
 >> Switch1 --- changed 3 --- failed 0 --- configure_vPC
^^^ completed: configure_vPC
vvv starting: configure_vPC_members
  > Switch2 --- changed 1 --- failed 0 --- Configure vPC member 1
  > Switch1 --- changed 1 --- failed 0 --- Configure vPC member 1
  > Switch2 --- changed 1 --- failed 0 --- Configure vPC member 2
 >> Switch2 --- changed 2 --- failed 0 --- configure_vPC_members
  > Switch1 --- changed 1 --- failed 0 --- Configure vPC member 2
 >> Switch1 --- changed 2 --- failed 0 --- configure_vPC_members
^^^ completed: configure_vPC_members
```
